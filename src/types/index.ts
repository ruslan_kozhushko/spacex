import React from 'react';
import { PaletteOptions } from '@material-ui/core/styles/createPalette';

export type FieldType = {
  label?: string | '',
  name: string,
  type?: string | 'text',
  placeholder?: string,
  component?: (object: any) => JSX.Element,
  render?: () => React.ComponentType,
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void,
};

export type RouteType = {
  path?: string,
  exact: boolean,
  component?: React.ComponentType | React.FC<any>,
  render?: () => React.ComponentType,
};

export type ThemeType = {
  palette: PaletteOptions,
};
