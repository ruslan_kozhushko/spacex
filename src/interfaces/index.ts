/** Review: а почему этот интерфейс одинокий такой?) */

export interface IUser {
  id?: string;
  name: string;
  password?: string;
  rocket?: string;
  twitter?: string;
}
