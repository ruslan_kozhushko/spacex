import React from 'react';
import { Link } from 'react-router-dom';
import {
  SwipeableDrawer,
  List,
  ListItem,
  ListItemText,
} from '@material-ui/core';
import useStyles from '../../styles';
import rocketIcon from '../../assets/rocket.svg';
import shipIcon from '../../assets/ship.svg';
import historyIcon from '../../assets/history.svg';
import launchpadIcon from '../../assets/launchpad.svg';

interface IProps {
  isDrawerOpened: boolean;
  toggleDrawer: () => void;
}

const NavigationDrawer: React.FC<IProps> = ({ isDrawerOpened, toggleDrawer }) => {
  const classes = useStyles();

  return (
    <SwipeableDrawer
      open={isDrawerOpened}
      onClose={toggleDrawer}
      onOpen={toggleDrawer}
      onKeyDown={toggleDrawer}
    >
      <List>
        <Link to="/ships" className={classes.learnMoreLink}>
          <ListItem button>
            <ListItemText>
              Ships
            </ListItemText>

            <img src={shipIcon} style={{ width: 30, height: 30 }} alt="ships" />
          </ListItem>
        </Link>

        <Link to="/rockets" className={classes.learnMoreLink}>
          <ListItem button>
            <ListItemText>
              Rockets
            </ListItemText>

            <img src={rocketIcon} style={{ width: 30, height: 30 }} alt="rockets" />
          </ListItem>
        </Link>

        <Link to="/launchpads" className={classes.learnMoreLink}>
          <ListItem button>
            <ListItemText>
              Launchpads
            </ListItemText>

            <img src={launchpadIcon} style={{ width: 30, height: 30 }} alt="launchpads" />
          </ListItem>
        </Link>

        <Link to="/histories" className={classes.learnMoreLink}>
          <ListItem button>
            <ListItemText>
              Histories
            </ListItemText>

            <img src={historyIcon} style={{ width: 30, height: 30 }} alt="histories" />
          </ListItem>
        </Link>
      </List>
    </SwipeableDrawer>
  );
};

export default NavigationDrawer;
