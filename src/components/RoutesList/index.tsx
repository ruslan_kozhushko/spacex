import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import {
  Container,
  Theme,
  useTheme,
  makeStyles,
  createStyles,
} from '@material-ui/core';
import Header from '../Header';
import routes from '../../routes';
import { RouteType } from '../../types';
import NavigationDrawer from '../NavigationDrawer';

const useStyles = makeStyles((theme: Theme) => createStyles({
  container: {
    display: 'flex',
    flex: 1,
    maxWidth: '100%',
    paddingRight: 0,
    paddingLeft: 0,
    backgroundColor: theme.palette.primary[theme.palette.type],
  },
}));

interface IProps {
  isDrawerOpened: boolean;
  toggleDrawer: () => void;
  switchTheme: () => void;
}

const RoutesList: React.FC<IProps> = ({
  isDrawerOpened,
  toggleDrawer,
  switchTheme,
}) => {
  const theme = useTheme();
  const classes = useStyles(theme);

  return (
    <Router>
      <Header
        toggleDrawer={toggleDrawer}
        switchTheme={switchTheme}
      />

      <Container className={classes.container}>
        <NavigationDrawer
          isDrawerOpened={isDrawerOpened}
          toggleDrawer={toggleDrawer}
        />

        <Switch>
          {routes.map((route: RouteType, index: number): JSX.Element => (
            <Route
              key={index}
              {...route}
            />
          ))}
        </Switch>
      </Container>
    </Router>
  );
};

export default RoutesList;
