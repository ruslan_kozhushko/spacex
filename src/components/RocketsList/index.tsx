import React, { useEffect, useReducer } from 'react';
import { useQuery } from '@apollo/react-hooks';
import {
  Theme,
  withTheme,
  CircularProgress,
  Typography,
} from '@material-ui/core';
import { loader } from 'graphql.macro';
import CustomList from '../../utils/CustomList';
import RocketCard from './RocketCard';
import rocketsReducer, { initialState } from '../../store/rockets/reducer';
import * as types from '../../store/rockets/types';
import useStyles from '../../styles';

const FETCH_ALL_ROCKETS = loader('../../graphql/rockets/rockets-list-query.gql');

interface IProps {
  theme: Theme;
}

const RocketsList: React.FC<IProps> = ({ theme }) => {
  const classes = useStyles(theme);

  const { loading, data, error } = useQuery(FETCH_ALL_ROCKETS);

  const [, dispatch] = useReducer(rocketsReducer, initialState);

  useEffect(() => {
    if (data) {
      dispatch({ type: types.FETCH_ALL_ROCKETS, payload: { data } });
    }
  }, [data]);

  if (loading) {
    return (
      <div className={classes.preloaderContainer}>
        <CircularProgress color="primary" />
      </div>
    );
  }

  if (error) {
    return (
      <div><span>{error.message}</span></div>
    );
  }

  if (data.rockets.length === 0) {
    return (
      <div className={classes.preloaderContainer}><Typography>There are no rockets yet!</Typography></div>
    );
  }

  return (
    <CustomList>
      {({ isListViewModeOn }) => data.rockets.map((rocket: any): JSX.Element => (
        <RocketCard
          key={rocket.id}
          {...rocket}
          isListViewModeOn={isListViewModeOn}
        />
      ))}
    </CustomList>
  );
};

export default withTheme(RocketsList);
