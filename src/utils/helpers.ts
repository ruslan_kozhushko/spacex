export const reduceText = (text: string): string => text.length > 100 ? `${text.slice(0, 99)}...` : text;
