export const FETCH_ALL_SHIPS = 'FETCH_ALL_SHIPS';
export const FETCH_SHIP = 'FETCH_SHIP';

interface IFetchAllShips {
  type: typeof FETCH_ALL_SHIPS;
  payload: {
    data: {
      ships: Array<object>,
    },
  };
}

interface IFetchShip {
  type: typeof FETCH_SHIP;
  payload: {
    data: {
      ship: object,
    },
  };
}

export type ShipsActionsTypes = IFetchAllShips | IFetchShip;
