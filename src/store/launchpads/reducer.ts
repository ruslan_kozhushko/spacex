import * as types from './types';

type InitialStateType = {
  items: Array<{}>,
  item: {},
};

export const initialState: InitialStateType = {
  items: [],
  item: {},
};

const launchpadsReducer = (state = initialState, action: types.LaunchpadsActionsTypes) => {
  switch (action.type) {
  case types.FETCH_ALL_LAUNCHPADS: {
    return {
      ...state,
      items: action.payload.data.launchpads,
    };
  }

  case types.FETCH_LAUNCHPAD: {
    return {
      ...state,
      item: action.payload.data.launchpad,
    };
  }

  default:
    return state;
  }
};

export default launchpadsReducer;
