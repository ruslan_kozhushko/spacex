import React from 'react';
import { useLocation, useHistory, useRouteMatch } from 'react-router-dom';
import {
  createStyles,
  makeStyles,
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Switch,
  useTheme,
  Theme,
} from '@material-ui/core';
import {
  Menu as MenuIcon,
  ArrowBack as ArrowBackIcon,
} from '@material-ui/icons';
import routes from '../../routes';
import SunIcon from '../../assets/sunny.svg';
import MoonIcon from '../../assets/moon.svg';

interface IProps {
  switchTheme: () => void;
  toggleDrawer: () => void;
}

const useStyles = makeStyles((theme: Theme) => createStyles({
  menuButton: {
    marginRight: 2,
  },
  title: {
    flexGrow: 1,
  },
  switcherContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  signInLink: {
    marginLeft: 15,
    textDecoration: 'none',
    color: '#fff',
    padding: 5,

    '&:hover': {
      borderRadius: 5,
      backgroundColor: '#3F00B5',
    },
  },
}));

const Header: React.FC<IProps> = ({
  switchTheme,
  toggleDrawer,
}) => {
  const theme = useTheme();
  const classes = useStyles(theme);

  const { pathname } = useLocation();

  const wrappedRoutes = routes.filter(route => route.path && route.path.match(/:id/)).map(route => route.path);

  const match = useRouteMatch({
    path: wrappedRoutes as Array<string>,
    strict: true,
    sensitive: true,
  });

  const history = useHistory();

  return (
    <AppBar position="static">
      <Toolbar>
        {pathname !== '/' && (
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
            onClick={match ? history.goBack : toggleDrawer}
          >
            {match ? <ArrowBackIcon /> : <MenuIcon />}
          </IconButton>
        )}

        <Typography variant="h6" className={classes.title}>
          Space X
        </Typography>

        <div className={classes.switcherContainer}>
          <img src={MoonIcon} style={{ width: 30, height: 30 }} alt="moon" />

          <Switch
            value="theme"
            color="default"
            checked={theme.palette.type === 'light'}
            onChange={switchTheme}
          />

          <img src={SunIcon} style={{ width: 30, height: 30 }} alt="sun" />
        </div>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
