import React from 'react';
import {
  Card,
  CardContent,
  CardActions,
  Typography,
  withTheme,
  Theme,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import { reduceText } from '../../utils/helpers';
import useStyles from '../../styles';

interface IProps {
  id: string;
  name: string;
  details: string;
  location: {
    region: string,
    name: string,
  };
  status: string;
  wikipedia: string;
  theme: Theme;
  isListViewModeOn: boolean;
}

const LaunchpadCard: React.FC<IProps> = ({
  id,
  name,
  details,
  location: {
    name: locationName,
    region,
  },
  status,
  theme,
  isListViewModeOn,
}) => {
  const classes = useStyles(theme);

  const cardDetails: string = reduceText(details);

  return (
    <Card className={!isListViewModeOn ? classes.listItem : classes.card}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary">{name}</Typography>

        <div className={classes.cardDetailsInfo}>
          <Typography>{cardDetails}</Typography>
          <Typography>Location: {`${locationName}, ${region}.`}</Typography>
          <Typography>Status: {status}</Typography>
        </div>
      </CardContent>

      <CardActions>
        <Link className={classes.learnMoreLink} to={`/launchpads/${id}`}>
          <Typography>Learn more</Typography>
        </Link>
      </CardActions>
    </Card>
  );
};

export default withTheme(LaunchpadCard);
