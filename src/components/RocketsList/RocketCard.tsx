import React from 'react';
import {
  Card,
  CardContent,
  CardActions,
  Typography,
  withTheme,
  Theme,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import { reduceText } from '../../utils/helpers';
import useStyles from '../../styles';

interface IProps {
  id: string;
  name: string;
  description: string;
  type: string;
  theme: Theme;
  isListViewModeOn: boolean;
}

const RocketCard: React.FC<IProps> = ({
  id,
  name,
  description,
  type,
  theme,
  isListViewModeOn,
}) => {
  const classes = useStyles(theme);

  const cardDetails: string = reduceText(description);

  return (
    <Card className={!isListViewModeOn ? classes.listItem : classes.card}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary">{name}</Typography>

        <div className={classes.cardDetailsInfo}>
          <Typography>Description: {cardDetails}</Typography>
          <Typography>Type: {type}</Typography>
        </div>
      </CardContent>

      <CardActions>
        <Link className={classes.learnMoreLink} to={`/rockets/${id}`}>
          <Typography>Learn more</Typography>
        </Link>
      </CardActions>
    </Card>
  );
};

export default withTheme(RocketCard);
