import React, { useEffect, useReducer } from 'react';
import { useParams } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import { QueryResult } from 'react-apollo';
import { loader } from 'graphql.macro';
import {
  CircularProgress,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
  Theme,
  withTheme,
} from '@material-ui/core';
import rocketsReducer, { initialState } from '../../store/rockets/reducer';
import * as types from '../../store/rockets/types';
import useStyles from '../../styles';

const FETCH_ROCKET = loader('../../graphql/rockets/rocket-query.gql');

interface IProps {
  theme: Theme;
}

const RocketPage: React.FC<IProps> = () => {
  const classes = useStyles();

  const { id } = useParams();

  const { loading, data, error }: QueryResult = useQuery(FETCH_ROCKET, {
    variables: { id },
  });

  const [, dispatch] = useReducer(rocketsReducer, initialState);

  useEffect(() => {
    if (data) {
      dispatch({ type: types.FETCH_ROCKET, payload: { data } });
    }
  }, [data]);

  if (loading) {
    return (
      <div className={classes.preloaderContainer}>
        <CircularProgress color="primary" />
      </div>
    );
  }

  if (error) {
    return (
      <div><span>{error.message}</span></div>
    );
  }

  const {
    rocket: {
      name,
      description,
      type,
      company,
      country,
      mass,
      diameter,
      height,
      active,
      boosters,
      cost_per_launch,
      engines: {
        engine_loss_max,
        layout,
        number,
        propellant_1,
        propellant_2,
        thrust_sea_level,
        thrust_to_weight,
        thrust_vacuum,
        type: engineType,
        version,
      },
      stages,
      success_rate_pct,
      wikipedia,
    },
  } = data;

  return (
    <Grid className={classes.pageContainer} container direction="column">
      <Paper>
        <Table aria-label="simple table">
          <TableBody>
            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Name:
              </TableCell>

              <TableCell>
                {name}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Description:
              </TableCell>

              <TableCell>
                {description}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Type:
              </TableCell>

              <TableCell>
                {type}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Company:
              </TableCell>

              <TableCell>
                {company}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Country:
              </TableCell>

              <TableCell>
                {country}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Mass:
              </TableCell>

              <TableCell>
                {`${mass.kg} kg / ${mass.lb} lb`}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Diameter:
              </TableCell>

              <TableCell>
                {`${diameter.feet} ft / ${diameter.meters} m`}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Height:
              </TableCell>

              <TableCell>
                {`${height.feet} ft / ${height.meters} m`}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Is active:
              </TableCell>

              <TableCell>
                {active ? 'active' : 'inactive'}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Boosters:
              </TableCell>

              <TableCell>
                {boosters}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Cost per launch:
              </TableCell>

              <TableCell>
                {`${cost_per_launch}$`}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Engines:
              </TableCell>

              <TableCell>
                <Typography>Engine loss max: {engine_loss_max};</Typography>
                <Typography>Layout: {layout};</Typography>
                <Typography>Number: {number};</Typography>
                <Typography>Propellant 1: {propellant_1};</Typography>
                <Typography>Propellant 2: {propellant_2};</Typography>
                <Typography>Thrust sea level: {`${thrust_sea_level.kN} kN / ${thrust_sea_level.lbf} lbf`};</Typography>
                <Typography>Thrust to weight: {thrust_to_weight};</Typography>
                <Typography>Thrust vacuum: {`${thrust_vacuum.kN} kN / ${thrust_vacuum.lbf} lbf`};</Typography>
                <Typography>Type: {engineType};</Typography>
                <Typography>Version: {version};</Typography>
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Stages:
              </TableCell>

              <TableCell>
                {stages}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Success rate pct:
              </TableCell>

              <TableCell>
                {success_rate_pct}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Wiki:
              </TableCell>

              <TableCell>
                <a href={wikipedia} target="_blank" rel="noreferrer noopener">
                  {wikipedia}
                </a>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    </Grid>
  );
};

export default withTheme(RocketPage);
