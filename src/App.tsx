import React, { useState } from 'react';
import { Provider } from 'react-redux';
import { ApolloProvider } from '@apollo/react-hooks';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';
import { pink, deepPurple } from '@material-ui/core/colors';
import client from './graphql';
import store from './store';
import RoutesList from './components/RoutesList';
import { ThemeType } from './types';

const themeObject: ThemeType = {
  palette: {
    primary: {
      light: '#d0e8e8',
      main: '#3f50b5',
      dark: '#1D232A',
      contrastText: '#fff',
    },
    secondary: {
      light: pink['A400'],
      main: '#f44336',
      dark: deepPurple['600'],
      contrastText: '#000',
    },
    type: localStorage.getItem('theme') && localStorage.getItem('theme') === 'light' ? 'light' : 'dark',
  },
};

const useDarkMode = () => {
  const [theme, setTheme] = useState(themeObject);

  const { palette: { type } } = theme;

  const toggleDarkMode = (): void => {
    const updatedTheme = {
      ...theme,
      palette: {
        ...theme.palette,
        type: type === 'light' ? 'dark' : 'light',
      },
    };

    localStorage.setItem('theme', updatedTheme.palette.type);

    setTheme(updatedTheme as ThemeType);
  };

  return [theme, toggleDarkMode];
};

const App = (): JSX.Element => {
  const [isDrawerOpened, setDrawerState] = useState(false);

  const [theme, toggleDarkMode] = useDarkMode();

  const themeConfig = createMuiTheme(theme as ThemeOptions);

  const toggleDrawer = (): void => setDrawerState(!isDrawerOpened);

  return (
    <ApolloProvider client={client}>
      <Provider store={store}>
        <MuiThemeProvider
          theme={{
            ...themeConfig,
            palette: {
              ...themeConfig.palette,
              type: localStorage.getItem('theme') || 'light',
            },
          }}
        >
          <RoutesList
            isDrawerOpened={isDrawerOpened}
            toggleDrawer={toggleDrawer}
            switchTheme={toggleDarkMode as (() => void)}
          />
        </MuiThemeProvider>
      </Provider>
    </ApolloProvider>
  );
};

export default App;
