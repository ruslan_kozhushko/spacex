import * as types from './types';

type InitialStateType = {
  items: Array<{}>;
  item: {};
};

export const initialState: InitialStateType = {
  items: [],
  item: {},
};

const rocketsReducer = (state = initialState, action: types.RocketsActionsTypes) => {
  switch (action.type) {
  case types.FETCH_ALL_ROCKETS: {
    return {
      ...state,
      items: action.payload.data.rockets,
    };
  }

  case types.FETCH_ROCKET: {
    return {
      ...state,
      item: action.payload.data.rocket,
    };
  }  

  default:
    return state;
  }
};

export default rocketsReducer;
