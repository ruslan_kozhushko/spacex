import React from 'react';
import {
  Formik,
  Form,
  Field,
  ErrorMessage,
} from 'formik';
import {
  FormGroup,
  withTheme,
  Theme,
} from '@material-ui/core';
import { FieldType } from '../types';
import useStyles from '../styles';

interface IProps {
  fields: Array<FieldType>;
  initialValues: object;
  validationSchema: object;
  formName?: string | '';
  formClassName?: string | '';
  formTitle?: string | '';
  handleSubmit: (values: any) => void;
  theme: Theme;
}

const CustomForm: React.FC<IProps> = ({
  fields,
  initialValues,
  validationSchema,
  formClassName,
  formName,
  formTitle,
  handleSubmit,
  theme,
}) => {
  const classes = useStyles(theme);

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={(values, { setSubmitting }) => {
        handleSubmit(values);
        setSubmitting(false);
      }}
    >
      {({ errors }: { errors: any }): JSX.Element => (
        <Form
          translate
          name={formName}
          className={formClassName}
        >
          {formTitle && formTitle.length !== 0 && <legend>{formTitle}</legend>}

          {fields.map((field, index) => (
            <FormGroup key={index}>
              <Field {...field} />

              {errors[field.name] && (
                <ErrorMessage
                  className={classes.errorMessage}
                  component="div"
                  name={field.name}
                />
              )}
            </FormGroup>
          ))}
        </Form>
      )}
    </Formik>
  );
};

export default withTheme(CustomForm);
