import * as types from './types';

type InitialStateType = {
  items: Array<{}>,
  item: {},
};

export const initialState: InitialStateType = {
  items: [],
  item: {},
};

const historiesReducer = (state = initialState, action: types.HistoriesActionsTypes) => {
  switch (action.type) {
  case types.FETCH_ALL_HISTORIES: {
    return {
      ...state,
      items: action.payload.data.items,
    };
  }

  case types.FETCH_HISTORY: {
    return {
      ...state,
      item: action.payload.data.item,
    };
  }

  default:
    return state;
  }
};

export default historiesReducer;
