import React, { useEffect, useReducer } from 'react';
import { useParams } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import { loader } from 'graphql.macro';
import {
  CircularProgress,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableRow,
} from '@material-ui/core';
import GoogleMapReact from 'google-map-react';
import launchpadsReducer, { initialState } from '../../store/launchpads/reducer';
import * as types from '../../store/launchpads/types';
import { GOOGLE_API_KEY } from '../../config/vars';
import useStyles from '../../styles';

const FETCH_LAUNCHPAD = loader('../../graphql/launchpads/launchpad-query.gql');

const LaunchpadPage: React.FC = () => {
  const classes = useStyles();

  const { id } = useParams();

  const { loading, data, error } = useQuery(FETCH_LAUNCHPAD, {
    variables: { id },
  });

  const [, dispatch] = useReducer(launchpadsReducer, initialState);

  useEffect(() => {
    if (data) {
      dispatch({ type: types.FETCH_LAUNCHPAD, payload: { data } });
    }
  }, [data]);

  if (loading) {
    return (
      <div className={classes.preloaderContainer}>
        <CircularProgress color="primary" />
      </div>
    );
  }

  if (error) {
    return (
      <div><span>{error.message}</span></div>
    );
  }

  const {
    launchpad: {
      name,
      details,
      status,
      attempted_launches,
      successful_launches,
      location,
      wikipedia,
    },
  } = data;

  return (
    <Grid className={classes.pageContainer} container direction="column">
      <Paper>
        <Table aria-label="simple table">
          <TableBody>
            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Name:
              </TableCell>

              <TableCell>
                {name}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Details:
              </TableCell>

              <TableCell>
                {details}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Status:
              </TableCell>

              <TableCell>
                {status}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Attempted launches:
              </TableCell>

              <TableCell>
                {attempted_launches}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Successful launches:
              </TableCell>

              <TableCell>
                {successful_launches}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Location:
              </TableCell>

              <TableCell>
                {`${location.region}, ${location.name}`}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Wiki:
              </TableCell>

              <TableCell>
                <a href={wikipedia} target="_blank" rel="noreferrer noopener">
                  {wikipedia}
                </a>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>

        <GoogleMapReact
          bootstrapURLKeys={{
            key: GOOGLE_API_KEY,
            region: location.region,
            language: 'en',
          }}
          defaultCenter={{
            lat: location.latitude,
            lng: location.longitude,
          }}
          defaultZoom={11}
          yesIWantToUseGoogleMapApiInternals
          draggable
        />
      </Paper>
    </Grid>
  );
};

export default LaunchpadPage;
