import { RouteType } from '../types';
import VehicleList from '../components/VehicleList';
import LaunchpadsList from '../components/LaunchpadsList';
import LaunchpadPage from '../components/LaunchpadsList/LaunchpadPage';
import ShipsList from '../components/ShipsList';
import ShipPage from '../components/ShipsList/ShipPage';
import RocketsList from '../components/RocketsList';
import RocketPage from '../components/RocketsList/RocketPage';
import HistoriesList from '../components/HistoriesList';
import HistoryPage from '../components/HistoriesList/HistoryPage';
import NotFound from '../utils/NotFound';

const routes: Array<RouteType> = [
  {
    path: '/',
    exact: true,
    component: VehicleList,
  }, {
    path: '/ships',
    exact: true,
    component: ShipsList,
  }, {
    path: '/ships/:id',
    exact: false,
    component: ShipPage,
  }, {
    path: '/rockets',
    exact: true,
    component: RocketsList,
  }, {
    path: '/rockets/:id',
    exact: false,
    component: RocketPage,
  }, {
    path: '/launchpads',
    exact: true,
    component: LaunchpadsList,
  }, {
    path: '/launchpads/:id',
    exact: false,
    component: LaunchpadPage,
  }, {
    path: '/histories',
    exact: true,
    component: HistoriesList,
  }, {
    path: '/histories/:id',
    exact: false,
    component: HistoryPage,
  }, {
    exact: true,
    component: NotFound,
  },
];

export default routes;
