import { makeStyles, createStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => createStyles({
  card: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    [theme.breakpoints.up('sm')]: {
      maxWidth: '20%',
    },
    [theme.breakpoints.down('sm')]: {
      maxWidth: '25%',
    },
    [theme.breakpoints.down(650)]: {
      maxWidth: '33%',
    },
    [theme.breakpoints.down(470)]: {
      maxWidth: '100%',
    },
    margin: 5,
    backgroundColor: theme.palette.type === 'dark' ? 'rgb(66, 66, 66)' : '#fff',
  },
  listItem: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    width: '100%',
    [theme.breakpoints.up('md')]: {
      maxWidth: 750,
    },
    [theme.breakpoints.down('md')]: {
      maxWidth: 550,
    },
    [theme.breakpoints.down('sm')]: {
      maxWidth: 470,
    },
    [theme.breakpoints.down(500)]: {
      maxWidth: 400,
    },
    [theme.breakpoints.down(425)]: {
      maxWidth: 350,
    },
    [theme.breakpoints.down(375)]: {
      maxWidth: 300,
    },
    margin: 5,
    backgroundColor: theme.palette.type === 'dark' ? 'rgb(66, 66, 66)' : '#fff',
  },
  title: {
    fontSize: 15,
    paddingBottom: 15,
  },
  learnMoreLink: {
    color: theme.palette.type === 'dark' ? '#fff' : '#00f',
    textDecoration: 'none',
    padding: 5,
    '&:hover': {
      backgroundColor: theme.palette.type === 'light' ? '#ccc' : '#000',
      borderRadius: 5,
    },
  },
  cardDetailsInfo: {
    color: theme.palette.type === 'dark' ? '#fff' : '#000',
  },
  preloaderContainer: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pageContainer: {
    padding: 15,
  },
  propName: {
    fontWeight: 'bold',
  },
  vehicleImage: {
    width: '100%',
    backgroundSize: 'cover',
  },
  switchListViewModeContainer: {
    [theme.breakpoints.down(500)]: {
      display: 'none',
    },
  },
  link: {
    textDecoration: 'none',
  },
  authButton: {
    marginTop: 15,
    color: '#fff',
  },
  errorMessage: {
    color: '#f00',
  },
}));

export default useStyles;
