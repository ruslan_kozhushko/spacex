import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import rocketsReducer from './rockets/reducer';
import shipsReducer from './ships/reducer';
import launchpadsReducer from './launchpads/reducer';
import historiesReducer from './histories/reducer';

const rootReducer = combineReducers({
  rockets: rocketsReducer,
  ships: shipsReducer,
  launchpads: launchpadsReducer,
  histories: historiesReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(
      logger,
    ),
  ),
);

export default store;
