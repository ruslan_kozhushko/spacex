export const FETCH_ALL_LAUNCHPADS = 'FETCH_ALL_LAUNCHPADS';
export const FETCH_LAUNCHPAD = 'FETCH_LAUNCHPAD';

interface IFetchAllLaunchpads {
  type: typeof FETCH_ALL_LAUNCHPADS;
  payload: {
    data: {
      launchpads: Array<object>,
    },
  };
}

interface IFetchLaunchpad {
  type: typeof FETCH_LAUNCHPAD;
  payload: {
    data: {
      launchpad: object,
    },
  };
}

export type LaunchpadsActionsTypes = IFetchAllLaunchpads | IFetchLaunchpad;
