export const FETCH_ALL_HISTORIES = 'FETCH_ALL_HISTORIES';
export const FETCH_HISTORY = 'FETCH_HISTORY';

interface IFetchAllHistories {
  type: typeof FETCH_ALL_HISTORIES;
  payload: {
    data: {
      items: Array<object>,
    },
  };
}

interface IFetchHistory {
  type: typeof FETCH_HISTORY;
  payload: {
    data: {
      item: object,
    },
  };
}

export type HistoriesActionsTypes = IFetchAllHistories | IFetchHistory;
