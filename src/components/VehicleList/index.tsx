import React from 'react';
import { Link } from 'react-router-dom';
import {
  Grid,
  createStyles,
  makeStyles,
  Theme,
  useTheme,
} from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => createStyles({
  '@keyframes blinker': {
    from: { opacity: 0 },
    to: { opacity: 1 },
  },
  linksList: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  link: {
    fontSize: 50,
    textDecoration: 'none',
    color: theme.palette.type === 'dark' ? '#fff' : '#000',
    '&:hover': {
      color: '#f0f',
      animationName: '$blinker',
      animationDuration: '1s',
      animationTimingFunction: 'linear',
      animationIterationCount:'infinite',
    },
  },
}));

const VehicleList: React.FC = () => {
  const theme = useTheme();
  const classes = useStyles(theme);

  return (
    <Grid className={classes.linksList} item xs={12}>
      <Link className={classes.link} to="/ships">Ships</Link>
      <Link className={classes.link} to="/rockets">Rockets</Link>
      <Link className={classes.link} to="/launchpads">Launchpads</Link>
      <Link className={classes.link} to="/histories">Histories</Link>
    </Grid>
  );
};

export default VehicleList;
