import React, { useEffect, useReducer } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { loader } from 'graphql.macro';
import { CircularProgress, Typography } from '@material-ui/core';
import CustomList from '../../utils/CustomList';
import LaunchpadCard from './LaunchpadCard';
import launchpadsReducer, { initialState } from '../../store/launchpads/reducer';
import * as types from '../../store/launchpads/types';
import useStyles from '../../styles';

const FETCH_ALL_LAUNCHPADS = loader('../../graphql/launchpads/launchpads-list-query.gql');

const LaunchpadsList: React.FC = () => {
  const classes = useStyles();

  const { loading, data, error } = useQuery(FETCH_ALL_LAUNCHPADS);

  const [, dispatch] = useReducer(launchpadsReducer, initialState);

  useEffect(() => {
    if (data) {
      dispatch({ type: types.FETCH_ALL_LAUNCHPADS, payload: { data } });
    }
  }, [data]);

  if (loading) {
    return (
      <div className={classes.preloaderContainer}>
        <CircularProgress color="primary" />
      </div>
    );
  }

  if (error) {
    return (
      <div><span>{error.message}</span></div>
    );
  }

  if (data.launchpads.length === 0) {
    return (
      <div className={classes.preloaderContainer}><Typography>There are no launchpads yet!</Typography></div>
    );
  }

  return (
    <CustomList>
      {({ isListViewModeOn }) => data.launchpads.map((launchpad: any): JSX.Element => (
        <LaunchpadCard
          key={launchpad.id}
          {...launchpad}
          isListViewModeOn={isListViewModeOn}
        />
      ))}
    </CustomList>
  );
};

export default LaunchpadsList;
