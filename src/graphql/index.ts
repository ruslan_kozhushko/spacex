import ApolloClient from 'apollo-boost';

export default new ApolloClient({
  uri: 'http://api.spacex.land/graphql/',
});
