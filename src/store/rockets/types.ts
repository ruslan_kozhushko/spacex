export const FETCH_ALL_ROCKETS = 'FETCH_ALL_ROCKETS';
export const FETCH_ROCKET = 'FETCH_ROCKET';

interface IFetchAllRockets {
  type: typeof FETCH_ALL_ROCKETS;
  payload: {
    data: {
      rockets: Array<object>,
    },
  };
}

interface IFetchRocket {
  type: typeof FETCH_ROCKET;
  payload: {
    data: {
      rocket: object,
    },
  };
}

export type RocketsActionsTypes = IFetchAllRockets | IFetchRocket;
