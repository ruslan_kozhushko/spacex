import React, { useEffect, useReducer } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { loader } from 'graphql.macro';
import {
  Theme,
  withTheme,
  CircularProgress,
  Typography,
} from '@material-ui/core';
import CustomList from '../../utils/CustomList';
import ShipCard from './ShipCard';
import shipsReducer, { initialState } from '../../store/ships/reducer';
import * as types from '../../store/ships/types';
import useStyles from '../../styles';

const FETCH_ALL_SHIPS = loader('../../graphql/ships/ships-list-query.gql');

interface IProps {
  theme: Theme;
}

const ShipsList: React.FC<IProps> = ({ theme }) => {
  const classes = useStyles(theme);

  const { loading, data, error } = useQuery(FETCH_ALL_SHIPS);

  const [, dispatch] = useReducer(shipsReducer, initialState);

  useEffect(() => {
    if (data) {
      dispatch({ type: types.FETCH_ALL_SHIPS, payload: { data } });
    }
  }, [data]);

  if (loading) {
    return (
      <div className={classes.preloaderContainer}>
        <CircularProgress color="primary" />
      </div>
    );
  }

  if (error) {
    return (
      <div><span>{error.message}</span></div>
    );
  }

  if (data.ships.length === 0) {
    return (
      <div className={classes.preloaderContainer}><Typography>There are no ships yet!</Typography></div>
    );
  }

  return (
    <CustomList>
      {({ isListViewModeOn }) => data.ships.map((ship: any) => (
        <ShipCard
          key={ship.id}
          {...ship}
          isListViewModeOn={isListViewModeOn}
        />
      ))}
    </CustomList>
  );
};

export default withTheme(ShipsList);
