import * as types from './types';

type InitialStateType = {
  items: Array<{}>;
  item: {};
};

export const initialState: InitialStateType = {
  items: [],
  item: {},
};

const shipsReducer = (state = initialState, action: types.ShipsActionsTypes) => {
  switch (action.type) {
  case types.FETCH_ALL_SHIPS: {
    return {
      ...state,
      items: action.payload.data.ships,
    };
  }

  case types.FETCH_SHIP: {
    return {
      ...state,
      item: action.payload.data.ship,
    };
  }

  default:
    return state;
  }
};

export default shipsReducer;
