import React from 'react';
import { Link } from 'react-router-dom';
import {
  Card,
  CardContent,
  CardActions,
  Typography,
  Theme,
  withTheme,
} from '@material-ui/core';
import useStyles from '../../styles';

interface IProps {
  id: string;
  name: string;
  model: string;
  type: string;
  image: string;
  url: string;
  theme: Theme;
  isListViewModeOn: boolean;
}

const ShipCard: React.FC<IProps> = ({
  id,
  name,
  model,
  type,
  image,
  url,
  theme,
  isListViewModeOn,
}) => {
  const classes = useStyles(theme);

  return (
    <Card className={!isListViewModeOn ? classes.listItem : classes.card}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary">{name}</Typography>

        <div className={classes.cardDetailsInfo}>
          <Typography>Model: {model || '-'}</Typography>
          <Typography>Type: {type}</Typography>

          <a href={url} target='_blank' rel="noopener noreferrer">
            <img
              src={image || 'https://image.freepik.com/free-vector/404_8024-4.jpg'}
              className={classes.vehicleImage}
              alt="ship"
            />
          </a>
        </div>
      </CardContent>

      <CardActions>
        <Link className={classes.learnMoreLink} to={`/ships/${id}`}>
          <Typography>Learn more</Typography>
        </Link>
      </CardActions>
    </Card>
  );
};

export default withTheme(ShipCard);
