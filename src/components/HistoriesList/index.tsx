import React, { useEffect, useReducer } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { loader } from 'graphql.macro';
import {
  Theme,
  withTheme,
  CircularProgress,
  Typography,
} from '@material-ui/core';
import CustomList from '../../utils/CustomList';
import HistoryCard from './HistoryCard';
import historiesReducer, { initialState } from '../../store/histories/reducer';
import * as types from '../../store/histories/types';
import useStyles from '../../styles';

const FETCH_ALL_HISTORIES = loader('../../graphql/histories/histories-list-query.gql');

interface IProps {
  theme: Theme;
}

const HistoriesList: React.FC<IProps> = ({ theme }) => {
  const classes = useStyles(theme);

  const { loading, data, error } = useQuery(FETCH_ALL_HISTORIES);

  const [, dispatch] = useReducer(historiesReducer, initialState);

  useEffect(() => {
    if (data) {
      dispatch({ type: types.FETCH_ALL_HISTORIES, payload: { data } });
    }
  }, [data]);

  if (loading) {
    return (
      <div className={classes.preloaderContainer}>
        <CircularProgress color="primary" />
      </div>
    );
  }

  if (error) {
    return (
      <div><span>{error.message}</span></div>
    );
  }

  if (data.histories.length === 0) {
    return (
      <div className={classes.preloaderContainer}><Typography>There are no histories yet!</Typography></div>
    );
  }

  return (
    <CustomList>
      {({ isListViewModeOn }) => data.histories.map((history: any): JSX.Element => (
        <HistoryCard
          key={history.id}
          {...history}
          isListViewModeOn={isListViewModeOn}
        />
      ))}
    </CustomList>
  );
};

export default withTheme(HistoriesList);
