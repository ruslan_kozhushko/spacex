import React, { useState, useCallback } from 'react';
import { Grid, IconButton } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faThList, faGripHorizontal } from '@fortawesome/free-solid-svg-icons';
import useStyles from '../styles';

interface IProps {
  children: React.FC<{ isListViewModeOn: boolean }>;
}

const CustomList: React.FC<IProps> = ({ children }) => {
  const classes = useStyles();

  const [isListViewModeOn, setViewMode] = useState(localStorage.getItem('isListViewModeOn') === 'false');

  const toggleViewMode = useCallback(() => {
    setViewMode(!isListViewModeOn);
    localStorage.setItem('isListViewModeOn', `${isListViewModeOn}`);
  }, [isListViewModeOn]);

  return (
    <Grid container direction="column">
      <Grid className={classes.switchListViewModeContainer} container justify="flex-end">
        <IconButton onClick={toggleViewMode}>
          <FontAwesomeIcon
            icon={isListViewModeOn ? faThList : faGripHorizontal}
            color="#3F50B5"
          />
        </IconButton>
      </Grid>

      <Grid
        container
        direction={isListViewModeOn ? 'row' : 'column'}
        justify="center"
        alignItems={isListViewModeOn ? undefined : 'center'}
      >
        {children({ isListViewModeOn })}
      </Grid>
    </Grid>
  );
};

export default CustomList;
