import React from 'react';
import {
  makeStyles,
  createStyles,
  Typography,
  withTheme,
  Theme,
} from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => createStyles({
  container: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.palette.primary[theme.palette.type],
  },
}));

interface IProps {
  theme: Theme;
}

const NotFound: React.FC<IProps> = ({ theme }) => {
  const classes = useStyles(theme);

  return (
    <div className={classes.container}>
      <Typography>ERROR 404. Page not found...</Typography>
    </div>
  );
};

export default withTheme(NotFound);
