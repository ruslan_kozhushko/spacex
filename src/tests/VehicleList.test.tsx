import React from 'react';
import renderer from 'react-test-renderer';
// import Enzyme, { shallow } from 'enzyme';
// import Adapter from 'enzyme-adapter-react-16';
import VehicleList from '../components/VehicleList';

// Enzyme.configure({
//   adapter: new Adapter(),
//   disableLifecycleMethods: true,
// });

// describe('<VehicleList />', () => {
//   it('Vehicle list renders without crashing', () => {
//     const vehicleListComponent = shallow(<VehicleList />);

//     expect(vehicleListComponent).toBeTruthy();
//   });
// });

test('Vehicle list renders without crashing', () => {
  const vehicleListComponent = renderer.create(<VehicleList />);

  const tree = vehicleListComponent.toJSON();

  expect(tree).toMatchSnapshot();
});
