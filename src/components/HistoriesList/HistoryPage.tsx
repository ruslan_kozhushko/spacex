import React, { useEffect, useReducer } from 'react';
import { useParams, Link } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import { loader } from 'graphql.macro';
import {
  CircularProgress,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
} from '@material-ui/core';
import historiesReducer, { initialState } from '../../store/histories/reducer';
import * as types from '../../store/histories/types';
import useStyles from '../../styles';

const FETCH_HISTORY = loader('../../graphql/histories/history-query.gql');

const HistoryPage: React.FC = () => {
  const classes = useStyles();

  const { id } = useParams();

  const { loading, data, error } = useQuery(FETCH_HISTORY, {
    variables: { id },
  });

  const [, dispatch] = useReducer(historiesReducer, initialState);

  useEffect(() => {
    if (data) {
      dispatch({ type: types.FETCH_HISTORY, payload: { data } });
    }
  }, [data]);

  if (loading) {
    return (
      <div className={classes.preloaderContainer}>
        <CircularProgress color="primary" />
      </div>
    );
  }

  if (error) {
    return (
      <div><span>{error.message}</span></div>
    );
  }

  const {
    history: {
      title,
      details,
      event_date_utc,
      links: {
        article,
        wikipedia,
        reddit,
        ...links
      },
      flight,
    },
  } = data;

  return (
    <Grid className={classes.pageContainer} container direction="column">
      <Paper>
        <Table aria-label="simple table">
          <TableBody>
            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Title:
              </TableCell>

              <TableCell>
                {title}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Details:
              </TableCell>

              <TableCell>
                {details}
              </TableCell>
            </TableRow>

            /** Review: а шо это такое и для чего оно нужно? */
            {!Object.values(links).every(item => !item) && (
              <TableRow>
                <TableCell className={classes.propName} component="th" scope="row">
                  Links:
                </TableCell>

                <TableCell>
                  {article && (
                    <a href={article} target="_blank" rel="noreferrer noopener" className={classes.link}>
                      Space X,
                    </a> 
                  )}

                  {reddit && (
                    <a href={reddit} target="_blank" rel="noreferrer noopener" className={classes.link}>
                      Reddit,
                    </a> 
                  )}

                  {wikipedia && (
                    <a href={wikipedia} target="_blank" rel="noreferrer noopener" className={classes.link}>
                      Wiki
                    </a>
                  )}
                </TableCell>
              </TableRow>
            )}

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Date of the event:
              </TableCell>

              <TableCell>
                {event_date_utc}
              </TableCell>
            </TableRow>

            {flight && (
              <TableRow>
                <TableCell className={classes.propName} component="th" scope="row">
                  Flight:
                </TableCell>

                <TableCell>
                  <Typography>Flight details: {flight.details}</Typography>

                  <Typography>
                    Rocket: 
                    <Link className={classes.link} to={`/rockets/${flight.rocket.rocket.id}`}>
                      {flight.rocket.rocket_name}
                    </Link>
                  </Typography>
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </Paper>
    </Grid>
  );
};

export default HistoryPage;
