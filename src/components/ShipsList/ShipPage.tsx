import React, { useEffect, useReducer } from 'react';
import { useParams } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import { loader } from 'graphql.macro';
import {
  Grid,
  Paper,
  Table,
  TableBody,
  TableRow,
  TableCell,
  CircularProgress,
} from '@material-ui/core';
import shipsReducer, { initialState } from '../../store/ships/reducer';
import * as types from '../../store/ships/types';
import useStyles from '../../styles';

const FETCH_SHIP = loader('../../graphql/ships/ship-query.gql');

type MissionType = {
  flight: string;
  name: string;
};

const ShipPage: React.FC = () => {
  const classes = useStyles();

  const { id } = useParams();

  const { loading, data, error } = useQuery(FETCH_SHIP, {
    variables: { id },
  });

  const [, dispatch] = useReducer(shipsReducer, initialState);

  useEffect(() => {
    if (data) {
      dispatch({ type: types.FETCH_SHIP, payload: { data } });
    }
  }, [data]);

  if (loading) {
    return (
      <div className={classes.preloaderContainer}>
        <CircularProgress color="primary" />
      </div>
    );
  }

  if (error) {
    return (
      <div><span>{error.message}</span></div>
    );
  }

  const {
    ship: {
      name,
      type,
      status,
      active,
      attempted_launches,
      successful_launches,
      speed_kn,
      roles,
      url,
      image,
      class: shipClass,
      weight_kg,
      weight_lbs,
      year_built,
      home_port,
      missions,
    },
  } = data;

  return (
    <Grid className={classes.pageContainer} container direction="column">
      <Paper>
        <Table aria-label="simple table">
          <TableBody>
            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Name:
              </TableCell>

              <TableCell>
                {name}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Type:
              </TableCell>

              <TableCell>
                {type}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Home port:
              </TableCell>

              <TableCell>
                {home_port}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Status:
              </TableCell>

              <TableCell>
                {status || '-'}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Is active:
              </TableCell>

              <TableCell>
                {active ? 'active' : 'inactive'}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Attempted launches:
              </TableCell>

              <TableCell>
                {attempted_launches || 0}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Successful launches:
              </TableCell>

              <TableCell>
                {successful_launches || 0}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Speed:
              </TableCell>

              <TableCell>
                {speed_kn || 0}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Roles:
              </TableCell>

              <TableCell>
                {roles.map((role: string, index: any): JSX.Element => (
                  <span key={index}>
                    {index === roles.length - 1 ? role : `${role}, `}
                  </span>
                ))}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Class:
              </TableCell>

              <TableCell>
                {shipClass}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Weight:
              </TableCell>

              <TableCell>
                {`${weight_kg} kg / ${weight_lbs} lbs`}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Year built:
              </TableCell>

              <TableCell>
                {year_built}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell className={classes.propName} component="th" scope="row">
                Missions:
              </TableCell>

              <TableCell>
                {missions.map((mission: MissionType, index: number): JSX.Element => (
                  <div key={index}>
                    <p>Name: {mission.name};</p>
                    <p>Amount of flights: {mission.flight};</p>
                  </div>
                ))}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>

        <a href={url} target='_blank' rel="noopener noreferrer">
          <img
            src={image || 'https://image.freepik.com/free-vector/404_8024-4.jpg'}
            className={classes.vehicleImage}
            alt="ship"
          />
        </a>
      </Paper>
    </Grid>
  );
};

export default ShipPage;
